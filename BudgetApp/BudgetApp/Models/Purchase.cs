﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace BudgetApp.Models
{
    public class Purchase
    {
		  [PrimaryKey, AutoIncrement]
		  public int Id { get; set; }

		  [Indexed]
		  public int MonthBudgetId { get; set; }

		  [MaxLength(50)]
		  public string Name { get; set; }

		  public float Price { get; set; }

		  [MaxLength(50)]
		  public string Date{ get; set; }

		  public PaymentMethodType PaymentMethodType { get; set; }

	 }
}
