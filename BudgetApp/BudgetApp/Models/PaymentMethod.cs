﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BudgetApp.Models
{
	 public enum PaymentMethodType
	 {
		  None = 0,
		  Cash = 1,
		  CreditCard = 2
	 }

	 public class PaymentMethod
	 {
		  public PaymentMethodType Type { get; private set; }

		  public PaymentMethod(PaymentMethodType type)
		  {
			   Type = type;
		  }

		  public override string ToString()
		  {
			   switch (Type)
			   {
					case PaymentMethodType.Cash:
						 {
							  return "Cash";
						 }
					case PaymentMethodType.CreditCard:
						 {
							  return "Credit Card";
						 }
			   }
			   return "";
		  }
	 }
}
