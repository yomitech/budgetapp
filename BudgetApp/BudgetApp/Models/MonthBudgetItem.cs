﻿using BudgetApp.ViewModel;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace BudgetApp.Models
{
    public class MonthBudgetItem
	{
		  [PrimaryKey, AutoIncrement]
		  public int Id { get; set; }

		  [Indexed]
		  public int UserId { get; set; }

		  public float TotalMoney { get; set; }

		  public float MoneySpent { get; set; }

		  public float BudgetLimit { get; set; }

		  public float StartingMoney { get; set; }

		  public string Date { get; set; }
	 }
}
