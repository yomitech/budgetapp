﻿using BudgetApp.View;
using System;
using System.Collections.Generic;
using System.Text;

namespace BudgetApp.Models
{
    public class PurchaseValidate
    {
		  private PurchaseViewModel _Purchase;

		  public PurchaseValidate(PurchaseViewModel purchase)
		  {
			   _Purchase = purchase;
		  }

		  bool ValidateName()
		  {
			   return (!string.IsNullOrWhiteSpace(_Purchase.Name));
		  }

		  bool ValidatePrice()
		  {
			   return _Purchase.Price > 0.0;
		  }

		  bool ValidatePaymentMethod()
		  {
			   return !(_Purchase.PaymentMethod.Type == PaymentMethodType.None);
		  }


		  bool Validate()
		  {



			   return true;
		  }
    }
}
