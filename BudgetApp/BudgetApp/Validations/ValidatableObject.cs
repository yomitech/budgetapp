﻿using BudgetApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace BudgetApp.Validations
{
    public class ValidatableObject<T> : ViewModelBase
    {
		  private readonly List<IValidationRule<T>> _Validations;
		  private List<string> _Errors;
		  private T _Value;
		  private bool _IsValid;

		  public List<IValidationRule<T>> Validations => _Validations;

		  public List<string> Errors
		  {
			   get
			   {
					return _Errors;
			   }

			   set
			   {
					SetProperty(ref _Errors, value);
			   }
		  }

		  public T Value
		  {
			   get
			   {
					return _Value;
			   }

			   set
			   {
					SetProperty(ref _Value, value);
			   }
		  }

		  public bool IsValid
		  {
			   get
			   {
					return _IsValid;
			   }

			   set
			   {
					SetProperty(ref _IsValid, value);
			   }
		  }

		  public ValidatableObject()
		  {
			   _IsValid = true;
			   _Errors = new List<string>();
			   _Validations = new List<IValidationRule<T>>();
		  }

		  public bool Validate()
		  {
			   Errors.Clear();

			   IEnumerable<string> errors = _Validations.Where(v => !v.Check(Value)).Select(v => v.ValidateMessage);

			   Errors = errors.ToList();

			   IsValid = !Errors.Any();

			   return IsValid;
		  }
	 }
}
