﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BudgetApp.Validations
{
    public interface IValidationRule<T>
    {
		  string ValidateMessage { get; set; }
		  bool Check(T value);
    }
}
