﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BudgetApp.Validations
{
	 public class IsNotNullOrEmptyRule<T> : IValidationRule<T>
	 {
		  public string ValidateMessage { get; set; }

		  public bool Check(T value)
		  {
			   if (value == null)
					return false;

			   var str = value as string;

			   var result = !(string.IsNullOrWhiteSpace(ValidateMessage) || str.Trim().Length == 0);

			   return result;
		  }
	 }
}
