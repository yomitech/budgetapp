﻿using BudgetApp.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace BudgetApp.Validations
{
	 public class IsNumericAndAboveZeroRule<T> : IValidationRule<T>
	 {
		  public string ValidateMessage { get; set; }

		  public bool Check(T value)
		  {
			   if (value == null)
					return false;

			   var num = value as string;

			   var isNumeric = float.TryParse(num, out float n);

			   return isNumeric && n > 0;
		  }
	 }
}
