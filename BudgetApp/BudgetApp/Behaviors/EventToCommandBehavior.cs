﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace BudgetApp.Behaviors
{
	 public class EventToCommandBehavior : BindableBehavior<Xamarin.Forms.View>
	 {
		  public static readonly BindableProperty EventNameProperty =
			  BindableProperty.Create("EventName", typeof(string), typeof(EventToCommandBehavior), null, BindingMode.OneWay);

		  public static readonly BindableProperty CommandProperty =
			  BindableProperty.Create("Command", typeof(ICommand), typeof(EventToCommandBehavior), null, BindingMode.OneWay);

		  public static readonly BindableProperty CommandParameterProperty =
			   BindableProperty.Create("CommandParameter", typeof(object), typeof(EventToCommandBehavior), null, BindingMode.OneWay);

		  public static readonly BindableProperty EventArgsConverterProperty =
			   BindableProperty.Create("EventArgsConverter", typeof(IValueConverter), typeof(EventToCommandBehavior), null, BindingMode.OneWay);

		  public static readonly BindableProperty EventArgsConverterParameterProperty =
			   BindableProperty.Create("EventArgsConverterParameter", typeof(object), typeof(EventToCommandBehavior), null, BindingMode.OneWay);

		  protected Delegate _Handler;
		  private EventInfo _EventInfo;

		  public string EventName
		  {
			   get { return (string)GetValue(EventNameProperty); }
			   set { SetValue(EventNameProperty, value); }
		  }

		  public ICommand Command
		  {
			   get { return (ICommand)GetValue(CommandProperty); }
			   set { SetValue(CommandProperty, value); }
		  }

		  public object CommandParameter
		  {
			   get { return GetValue(CommandParameterProperty); }
			   set { SetValue(CommandParameterProperty, value); }
		  }

		  public IValueConverter EventArgsConverter
		  {
			   get { return (IValueConverter)GetValue(EventArgsConverterProperty); }
			   set { SetValue(EventArgsConverterProperty, value); }
		  }

		  public object EventArgsConverterParameter
		  {
			   get { return GetValue(EventArgsConverterParameterProperty); }
			   set { SetValue(EventArgsConverterParameterProperty, value); }
		  }

		  protected override void OnAttachedTo(Xamarin.Forms.View bindable)
		  {
			   base.OnAttachedTo(bindable);

			   var events = AssociatedObject.GetType().GetRuntimeEvents().ToArray();
			   if(events.Any())
			   {
					_EventInfo = events.FirstOrDefault(e => e.Name == EventName);
					if(_EventInfo == null)
					{
						 throw new ArgumentException(string.Format("EventToCommand: Can't find any event named '{0}' on attached type", EventName));
					}
			   }

			   AddEventHandler(_EventInfo, AssociatedObject, OnFired);
		  }

		  private void AddEventHandler(EventInfo eventInfo, object item, Action<object, EventArgs> action)
		  {
			   var eventParameters = eventInfo.EventHandlerType.GetRuntimeMethods().First(m => m.Name == "Invoke").GetParameters()
					.Select(p => Expression.Parameter(p.ParameterType)).ToArray();

			   var actionInvoke = action.GetType().GetRuntimeMethods().First(m => m.Name == "Invoke");

			   _Handler = Expression.Lambda(eventInfo.EventHandlerType,
								   Expression.Call(Expression.Constant(action), actionInvoke, eventParameters[0], eventParameters[1]),
								   eventParameters).Compile();

			   eventInfo.AddEventHandler(item, _Handler);
		  }

		  private void OnFired(object sender, EventArgs eventArgs)
		  {
			   if( Command == null)
			   {
					return;
			   }

			   var parameter = CommandParameter;

			   if(eventArgs != null && eventArgs != EventArgs.Empty)
			   {
					parameter = eventArgs;

					if(EventArgsConverter != null)
					{
						 parameter = EventArgsConverter.Convert(eventArgs, typeof(object), EventArgsConverterParameter, CultureInfo.CurrentUICulture);
					}

					if(Command.CanExecute(parameter))
					{
						 Command.Execute(parameter);
					}
			   }
		  }
	 }
}
