﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace BudgetApp.Effects
{
	 public class EntryLineColorEffect : RoutingEffect
	 {
		  public EntryLineColorEffect() : base("BudgetApp.EntryLineColorEffect")
		  {
		  }
	 }
}
