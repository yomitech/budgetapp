﻿using BudgetApp.Models;
using BudgetApp.Services;
using BudgetApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BudgetApp.View
{
	 [XamlCompilation(XamlCompilationOptions.Compile)]
	 public partial class AddPurchaseFormPage : ContentPage
	 {
		  public AddPurchaseFormPage(MonthBudgetItemViewModel monthBudget = null)
		  {
			   InitializeComponent();
			   BindingContext = new AddPurchaseFormViewModel(ViewModelLocator.Resolve<IPageService>(),
					ViewModelLocator.Resolve<IDatabaseService>(), monthBudget);
		  }
	 }
}