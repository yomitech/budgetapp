﻿using BudgetApp.Services;
using BudgetApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BudgetApp.View
{
	 [XamlCompilation(XamlCompilationOptions.Compile)]
	 public partial class MonthBudgetItemDetailPage : ContentPage
	 {
		  private MonthBudgetItemDetailViewModel _ViewModel;
		  public MonthBudgetItemDetailPage(MonthBudgetItemViewModel monthBudgetItem)
		  {
			   InitializeComponent();

			   _ViewModel = new MonthBudgetItemDetailViewModel(monthBudgetItem, ViewModelLocator.Resolve<IPageService>(), ViewModelLocator.Resolve<IDatabaseService>());

			   BindingContext = _ViewModel;
		  }

		  protected override void OnAppearing()
		  {
			   base.OnAppearing();
	 
			   _ViewModel.LoadPurchasesCommand.Execute(null);
		  }
	 }
}