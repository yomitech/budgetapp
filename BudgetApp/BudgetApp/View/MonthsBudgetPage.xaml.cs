﻿using BudgetApp.Services;
using BudgetApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BudgetApp.View
{
	 [XamlCompilation(XamlCompilationOptions.Compile)]
	 public partial class MonthsBudgetPage : ContentPage
	 {
		  private MonthsBudgetViewModel _ViewModel;

		  public MonthsBudgetPage()
		  {
			   InitializeComponent();

			   _ViewModel = new MonthsBudgetViewModel(ViewModelLocator.Resolve<IPageService>(), ViewModelLocator.Resolve<IDatabaseService>());

			   BindingContext = _ViewModel;
		  }

		  protected override void OnAppearing()
		  {
			   base.OnAppearing();

			   _ViewModel.LoadMonthsCommand.Execute(null);
		  }
	 }
}