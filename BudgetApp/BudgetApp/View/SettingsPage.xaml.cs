﻿using BudgetApp.Services;
using BudgetApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BudgetApp.View
{
	 [XamlCompilation(XamlCompilationOptions.Compile)]
	 public partial class SettingsPage : ContentPage
	 {
		  public SettingsPage()
		  {
			   InitializeComponent();
			   BindingContext = new SettingsViewModel(ViewModelLocator.Resolve<IDatabaseService>(), ViewModelLocator.Resolve<IPageService>());
		  }

		  private void Button_Clicked(object sender, EventArgs e)
		  {
			   facebookLoginButton.LogoutRequest(this, e);
		  }
	 }
}