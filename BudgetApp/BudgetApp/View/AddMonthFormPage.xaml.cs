﻿using BudgetApp.Services;
using BudgetApp.ViewModel;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BudgetApp.View
{
	 [XamlCompilation(XamlCompilationOptions.Compile)]
	 public partial class AddMonthFormPage : ContentPage
	 {
		  public AddMonthFormPage()
		  {
			   InitializeComponent();
			   BindingContext = new AddMonthFormViewModel(ViewModelLocator.Resolve<IPageService>(), ViewModelLocator.Resolve<IDatabaseService>());
		  }
	 }
}