﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;



namespace BudgetApp.Helpers
{
	 /// <summary>
	 /// This is the Settings static class that can be used in your Core solution or in any
	 /// of your client applications. All settings are laid out the same exact way with getters
	 /// and setters. 
	 /// </summary>
	 public static class Settings
	 {
		  private static ISettings AppSettings
		  {
			   get
			   {
					return CrossSettings.Current;
			   }
		  }

		  #region Setting Constants

		  private const string SettingsKey = "settings_key";
		  private static readonly string SettingsDefault = string.Empty;
		  private static readonly int DailyNotificationHourDefault = 19;
		  private static readonly int DailyNotificationMinuteDefault = 0;
		  private static readonly int DefaultUserId = -1;

		  #endregion


		  public static string GeneralSettings
		  {
			   get
			   {
					return AppSettings.GetValueOrDefault(SettingsKey, SettingsDefault);
			   }
			   set
			   {
					AppSettings.AddOrUpdateValue(SettingsKey, value);
			   }
		  }

		  public static string FacebookAccessToken
		  {
			   get => AppSettings.GetValueOrDefault(nameof(FacebookAccessToken), string.Empty);
			   set => AppSettings.AddOrUpdateValue(nameof(FacebookAccessToken), value);
		  }

		  public static int DailyNotificationHour
		  {
			   get => AppSettings.GetValueOrDefault(nameof(DailyNotificationHour), DailyNotificationHourDefault);
			   set => AppSettings.AddOrUpdateValue(nameof(DailyNotificationHour), value);
		  }

		  public static int DailyNotificationMinute
		  {
			   get => AppSettings.GetValueOrDefault(nameof(DailyNotificationMinute), DailyNotificationMinuteDefault);
			   set => AppSettings.AddOrUpdateValue(nameof(DailyNotificationMinute), value);
		  }

		  public static bool IsLoggedIn
		  {
			   get => AppSettings.GetValueOrDefault(nameof(IsLoggedIn), false);
			   set => AppSettings.AddOrUpdateValue(nameof(IsLoggedIn), value);
		  }

		  public static int UserId
		  {
			   get => AppSettings.GetValueOrDefault(nameof(UserId), DefaultUserId);
			   set => AppSettings.AddOrUpdateValue(nameof(UserId), value);
		  }

	 }
}
