﻿using BudgetApp.Helpers;
using BudgetApp.Services;
using BudgetApp.View;
using BudgetApp.ViewModel;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace BudgetApp
{
	 public partial class App : Application
	 {
		  static private string _pageName;

		  private readonly ILocalNotification _LocalNotificationService;

		  public App(string page = "Main")
		  {
			   InitializeComponent();

			   // Inversion of control
			   ViewModelLocator.RegisterDependencies();

			   _LocalNotificationService = DependencyService.Get<ILocalNotification>();

			   _pageName = page;

			   MainPage = new LoginPage();

			   if (Settings.IsLoggedIn)
			   {
					_LocalNotificationService.ShowNotificationEveryDay(Settings.DailyNotificationHour, Settings.DailyNotificationMinute);

					GoToPage();
			   }

		  }

		  public static void GoToPage()
		  {
			   switch (_pageName)
			   {
					case "Main":
						 Current.MainPage = new NavigationPage(new MainPage());
						 break;
					case "AddPurchaseFormPage":
						 Task.Run(async () => { await Current.MainPage.Navigation.PushModalAsync(new AddPurchaseFormPage()); }).Wait();
						 break;
			   }
		  }

		  protected override void OnStart()
		  {
			   // Handle when your app starts
		  }

		  protected override void OnSleep()
		  {
			   // Handle when your app sleeps
		  }

		  protected override void OnResume()
		  {
			   // Handle when your app resumes
		  }
		  
	 }
}
