﻿using BudgetApp.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using SQLite;
using BudgetApp.Services;
using System.Linq;
using System.Threading.Tasks;
using BudgetApp.Validations;
using BudgetApp.Helpers;

namespace BudgetApp.ViewModel
{
	 public class AddMonthFormViewModel : ViewModelBase
	 {
		  private string _Month;
		  private int _Year;
		  private bool _TakeFromLastMonth;
		  private DateTime _Date;

		  private readonly IPageService _PageService;
		  private readonly IDatabaseService _DatabaseService;

		  private List<MonthBudgetItem> _Months;

		  private ValidatableObject<string> _BudgetLimitValidation;
		  private ValidatableObject<string> _StartingMoneyValidation;
		  private bool _DateErrorValid;
		  private string _DateErrorString;

		  public string Month
		  {
			   get { return _Month; }
			   set
			   {
					if (SetProperty(ref _Month, value))
					{
						 UpdateMonth();
						 ValidateDate();
					}
			   }
		  }

		  public int Year
		  {
			   get { return _Year; }
			   set
			   {
					if (SetProperty(ref _Year, value))
					{
						 UpdateYear();
						 ValidateDate();
					}
			   }
		  }

		  public DateTime Date
		  {
			   get { return _Date; }
			   set { SetProperty(ref _Date, value); }
		  }

		  public bool TakeFromLastMonth
		  {
			   get { return _TakeFromLastMonth; }
			   set
			   {
					if(SetProperty(ref _TakeFromLastMonth, value))
					{
						 if (_TakeFromLastMonth)
						 {
							  TakeInfoFromLastMonth();
						 }
					}
			   }
		  }

		  public ValidatableObject<string> BudgetLimitValidation
		  {
			   get { return _BudgetLimitValidation; }
			   set
			   {
					SetProperty(ref _BudgetLimitValidation, value);
			   }
		  }

		  public ValidatableObject<string> StartingMoneyValidation
		  {
			   get { return _StartingMoneyValidation; }
			   set
			   {
					SetProperty(ref _StartingMoneyValidation, value);
			   }
		  }

		  public bool DateErrorValid
		  {
			   get { return _DateErrorValid; }
			   set { SetProperty(ref _DateErrorValid, value); }
		  }

		  public string DateErrorString
		  {
			   get { return _DateErrorString; }
			   set { SetProperty(ref _DateErrorString, value); }
		  }


		  public ICommand AddMonthCommand { get; private set; }
		  public ICommand ValidateBudgetLimitCommand { get; private set; }
		  public ICommand ValidateStartingMoneyCommand { get; private set; }
		  public ICommand StartingMoneyFocusedCommand { get; private set; }

		  public AddMonthFormViewModel(IPageService pageService, IDatabaseService databaseService)
		  {
			   _PageService = pageService;
			   _DatabaseService = databaseService;

			   _Months = new List<MonthBudgetItem>();

			   Task.Run(async () => { _Months = await _DatabaseService.GetTable<MonthBudgetItem>(); }).Wait();

			   Month = DateTime.Now.ToString("MMMM", CultureInfo.GetCultureInfo("en-US"));
			   Year = DateTime.Now.Year;


			   BudgetLimitValidation = new ValidatableObject<string>();
			   StartingMoneyValidation = new ValidatableObject<string>();

			   BudgetLimitValidation.Value = "0.0";
			   StartingMoneyValidation.Value = "0.0";

			   AddMonthCommand = new Command(async () =>
			   {
					bool IsValid = Validate();

					if (IsValid)
					{
						 var newMonthBudget = new MonthBudgetItem
						 {
							  UserId = Settings.UserId,
							  StartingMoney = float.Parse(StartingMoneyValidation.Value),
							  BudgetLimit = float.Parse(BudgetLimitValidation.Value),
							  Date = Date.ToString()
						 };

						 await _DatabaseService.InsertAsync(newMonthBudget);

						 await _PageService.PopModalAsync();
					}
			   });

			   StartingMoneyFocusedCommand = new Command(() =>
			   {
					if(StartingMoneyValidation.Value == "0.0")
					{
						 StartingMoneyValidation.Value = "";
					}

			   });


			   ValidateBudgetLimitCommand = new Command(()=> { ValidateBudgetLimit(); });
			   ValidateStartingMoneyCommand = new Command(() => { ValidateStartingMoney(); });

			   AddValidations();
		  }

		  private bool CheckMonthAlreadyExists(string date)
		  {
			   var exisitingMonth = (_Months).Where(m => m.Date == date).FirstOrDefault();

			   return (exisitingMonth != null);
		  }

		  private bool CheckDateIsInTheFuture()
		  {
			   var now = DateTime.Now;
			   
			   if(Date.Year > now.Year)
			   {
					return true;
			   }
			   else if(Date.Month > now.Month && Date.Year == now.Year)
			   {
					return true;
			   }

			   return false;

		  }

		  private bool CheckIfStartingMoneyIsBiggerThanBudgetLimit()
		  {
			   var startingMoney = float.Parse(StartingMoneyValidation.Value);
			   var budgetLimit = float.Parse(BudgetLimitValidation.Value);

			   var result = startingMoney > budgetLimit;

			   if (!result)
			   {
					BudgetLimitValidation.Errors.Add("The budget can't be above your starting money!");
					BudgetLimitValidation.IsValid = false;
			   }

			   return (result);
		  }

		  private void UpdateMonth()
		  {
			   if (!string.IsNullOrEmpty(Month))
			   {
					int monthNumber = DateTime.ParseExact(Month, "MMMM", CultureInfo.InvariantCulture).Month;
					Date = new DateTime(Date.Year, monthNumber, 1);
			   }
		  }

		  private void UpdateYear()
		  {
			   if (Year > 0)
			   {
					Date = new DateTime(Year, Date.Month, 1);
			   }
		  }

		  private void TakeInfoFromLastMonth()
		  {
			   int monthNumber = Date.Month;
			   int yearNumber = Date.Year;

			   monthNumber--;
			   if (monthNumber == 0)
			   {
					monthNumber = 12;
					yearNumber -= 1;
			   }

			   DateTime newDate = new DateTime(yearNumber, monthNumber, 1);
			   var lastMonth = (_Months).Where(m => m.Date == newDate.ToString()).FirstOrDefault();

			   ValidateLastMonthInfo(lastMonth);
		  }

		  private void ValidateLastMonthInfo(MonthBudgetItem lastMonth)
		  {

			   if (lastMonth != null)
			   {
					if (lastMonth.StartingMoney > 0)
					{
						 StartingMoneyValidation.Value = lastMonth.TotalMoney.ToString();
					}
					else
					{
						 WrongLastMonthInfo("Starting money of last month is zero");
					}
			   }
			   else
			   {
					WrongLastMonthInfo("Starting money of last month does not exist");
			   }

		  }

		  private void WrongLastMonthInfo(string message)
		  {
			   ValidateStartingMoney();

			   // This message should be displayed first
			   StartingMoneyValidation.Errors.Insert(0, message);
			   StartingMoneyValidation.IsValid = false;

			   TakeFromLastMonth = false;
		  }

		  private bool Validate()
		  {
			   bool isBudgetLimitValid = ValidateBudgetLimit();
			   bool isStartingMoneyValid = ValidateStartingMoney();
			   bool isDateValid = ValidateDate();

			   return isBudgetLimitValid && isStartingMoneyValid && isDateValid;
		  }

		  private bool ValidateBudgetLimit()
		  {
			   return BudgetLimitValidation.Validate() && CheckIfStartingMoneyIsBiggerThanBudgetLimit();
		  }

		  private bool ValidateStartingMoney()
		  {
			   return StartingMoneyValidation.Validate();
		  }

		  private bool ValidateDate()
		  {
			   DateErrorValid = false;
			   if (CheckMonthAlreadyExists(Date.ToString()))
			   {
					DateErrorString = $"This date already exists.";
					
			   }
			   else if(CheckDateIsInTheFuture())
			   {
					DateErrorString = $"This date has not yet arrived.";
			   }
			   else
			   {
					DateErrorString = "";
					DateErrorValid = true;
			   }

			   return DateErrorValid;
		  }

		  private void AddValidations()
		  {
			   BudgetLimitValidation.Validations.Add(new IsNumericAndAboveZeroRule<string>() { ValidateMessage = "A budget limit has to be a number and above zero."});
			   StartingMoneyValidation.Validations.Add(new IsNumericAndAboveZeroRule<string>() { ValidateMessage = "A starting money has to be a number and above zero." });
		  }
	 }
}