﻿using Autofac;
using BudgetApp.Services;

namespace BudgetApp.ViewModel
{
	 // Change the name to service locator?
	 public static class ViewModelLocator
	 {
		  private static IContainer _Container;

		  public static void RegisterDependencies()
		  {
			   var builder = new ContainerBuilder();

			   // Register here all your dependencies
			   builder.RegisterType<PageService>().As<IPageService>();
			   builder.RegisterType<SqliteDatabaseService>().As<IDatabaseService>();

			   _Container = builder.Build();
		  }

		  public static T Resolve<T>() where T : class
		  {
			   return _Container.Resolve<T>();
		  }
	 }
}
