﻿using BudgetApp.Models;
using BudgetApp.Services;
using System.Threading.Tasks;
using System.Linq;
using BudgetApp.Helpers;
using Xamarin.Forms;
using BudgetApp.View;
using System;

namespace BudgetApp.ViewModel
{
	 public class SettingsViewModel : ViewModelBase
	 {
		  private User _CurrentUser;
		  private TimeSpan _NotificationTime;
		  private readonly IDatabaseService _DatabaseService;
		  private readonly IPageService _PageService;
		  private readonly ILocalNotification _LocalNotificationService;


		  public Command LogoutCommand { get; private set; }
		  public Command SaveNotificationTimeCommand { get; private set; }


		  public User CurrentUser
		  {
			   get { return _CurrentUser; }
			   set { SetProperty(ref _CurrentUser, value); }
		  }

		  public TimeSpan NotificationTime
		  {
			   get { return _NotificationTime; }
			   set
			   {
					if (SetProperty(ref _NotificationTime, value))
					{

					}
			   }
		  }



		  public SettingsViewModel(IDatabaseService databaseService, IPageService pageService)
		  {
			   _DatabaseService = databaseService;
			   _PageService = pageService;
			   _LocalNotificationService = DependencyService.Get<ILocalNotification>();


			   NotificationTime = new TimeSpan(Settings.DailyNotificationHour, Settings.DailyNotificationMinute, 0);

			   Task.Run(async () => {
					CurrentUser = (await _DatabaseService.GetTable<User>()).Where(u => u.Id == Settings.UserId).FirstOrDefault();


			   }).Wait();

			   LogoutCommand = new Command(async () =>
			   {

					if(Settings.IsLoggedIn)
					{
						 Settings.UserId = -1;
						 Settings.IsLoggedIn = false;
						 Settings.FacebookAccessToken = string.Empty;

						 // Logout from Facebook

						 await _PageService.PopAsync();
						 await _PageService.PushAsync(new LoginPage());
					}

			   });

			   SaveNotificationTimeCommand = new Command(() => {
					Settings.DailyNotificationHour = _NotificationTime.Hours;
					Settings.DailyNotificationMinute = _NotificationTime.Minutes;

					_LocalNotificationService.ShowNotificationEveryDay(Settings.DailyNotificationHour, Settings.DailyNotificationMinute);
			   });
		  }
	 }
}
