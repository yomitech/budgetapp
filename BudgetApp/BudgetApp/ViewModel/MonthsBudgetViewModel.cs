﻿using BudgetApp.Models;
using BudgetApp.Services;
using BudgetApp.View;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using BudgetApp.Helpers;

namespace BudgetApp.ViewModel
{
	 public class MonthsBudgetViewModel : ViewModelBase
    {
		  private ObservableCollection<MonthBudgetItemViewModel> _Months;
		  public ObservableCollection<MonthBudgetItemViewModel> _DisplayMonths;
		  private MonthBudgetItemViewModel _SelectedMonthBudgetItem;
		  private string _SearchQuery;
		  private bool _IsSearchLoading;
		  private string _StatusString;
		  private readonly IPageService _PageService;
		  private readonly IDatabaseService _DatabaseService;


		  public ObservableCollection<MonthBudgetItemViewModel> DisplayMonths
		  {
			   get { return _DisplayMonths; }
			   set
			   {
					SetProperty(ref _DisplayMonths, value);
			   }
		  }

		  public MonthBudgetItemViewModel SelectedMonthBudgetItem
		  {
			   get { return _SelectedMonthBudgetItem; }
			   set
			   {
					if(SetProperty(ref _SelectedMonthBudgetItem, value))
					{
						 HandleSelectedItem();
					}
			   }
		  }

		  public string SearchQuery
		  {
			   get { return _SearchQuery; }
			   set
			   {
					if(SetProperty(ref _SearchQuery, value))
					{
						 if(string.IsNullOrEmpty(_SearchQuery) || string.IsNullOrWhiteSpace(_SearchQuery))
						 {
							  DisplayMonths = new ObservableCollection<MonthBudgetItemViewModel>(_Months);
						 }
					}


			   }
		  }

		  public bool IsSearchLoading
		  {
			   get { return _IsSearchLoading; }
			   set
			   {
					SetProperty(ref _IsSearchLoading, value);
			   }
		  }

		  public string StatusString
		  {
			   get { return _StatusString; }
			   set { SetProperty(ref _StatusString, value); }
		  }

		  public ICommand AddMonthCommand{ get; private set; }
		  public ICommand LoadMonthsCommand{ get; private set; }
		  public ICommand SearchMonthCommand { get; private set; }

		  public MonthsBudgetViewModel(IPageService pageService, IDatabaseService databaseService)
		  {
			   _PageService = pageService;
			   _DatabaseService = databaseService;


			   DisplayMonths = new ObservableCollection<MonthBudgetItemViewModel>();
			   _Months = new ObservableCollection<MonthBudgetItemViewModel>();

			   Task.Run(async ()=> { await LoadMonthsFromDB(); }).Wait();

			   AddMonthCommand = new Command(()=> {

					IsSearchLoading = false;
					SearchQuery = string.Empty;

					_PageService.PushModalAsync(new AddMonthFormPage());

			   });

			   LoadMonthsCommand = new Command(async ()=> { await LoadMonthsFromDB(); });

			   SearchMonthCommand = new Command(()=> {

					IsSearchLoading = true;

					var months = new ObservableCollection<MonthBudgetItemViewModel>();

					foreach(var item in _Months)
					{
						 if(item.Date.ToString("MMMM yyyy").ToLower().Contains(SearchQuery.ToLower()))
						 {
							  months.Add(item);
						 }
					}

					DisplayMonths.Clear();
					DisplayMonths = new ObservableCollection<MonthBudgetItemViewModel>(months);

					if(DisplayMonths.Count() == 0)
					{
						 StatusString = "No results found.";
					}
					else
					{
						 StatusString = "";
					}

					IsSearchLoading = false;
			   });
		  }

		  private void HandleSelectedItem()
		  {
			   if (SelectedMonthBudgetItem != null)
			   {
					_PageService.PushAsync(new MonthBudgetItemDetailPage(SelectedMonthBudgetItem));
					SelectedMonthBudgetItem = null;
			   }
		  }

		  private async Task LoadMonthsFromDB()
		  {
			   var monthsFromDB = (await _DatabaseService.GetTable<MonthBudgetItem>()).Where(m => m.UserId == Settings.UserId).ToList();

			   ObservableCollection<MonthBudgetItemViewModel> months = new ObservableCollection<MonthBudgetItemViewModel>();

			   for(int i = 0; i < monthsFromDB.Count; ++i)
			   {
					var month = monthsFromDB[i];
					var monthBudgetItemViewModel = new MonthBudgetItemViewModel
					{
						 Id = month.Id,
						 Date = DateTime.Parse(month.Date),
						 BudgetLimit = month.BudgetLimit,
						 MoneySpent = month.MoneySpent,
						 StartingMoney = month.StartingMoney,
					};

					if (monthBudgetItemViewModel.TotalMoney == 0 && monthBudgetItemViewModel.StartingMoney > 0)
					{
						 monthBudgetItemViewModel.TotalMoney = monthBudgetItemViewModel.StartingMoney - monthBudgetItemViewModel.MoneySpent;
						 await _DatabaseService.QueryAsync<MonthBudgetItem>($"UPDATE MonthBudgetItem SET TotalMoney={monthBudgetItemViewModel.TotalMoney}" +
							  $" WHERE ID={monthBudgetItemViewModel.Id}");
					}

					months.Add(monthBudgetItemViewModel);
			   }

			   _Months.Clear();
			   _Months = new ObservableCollection<MonthBudgetItemViewModel>(months.OrderByDescending(m => m.Date));

			   if(_Months.Count() == 0)
			   {
					StatusString = "No months are listed.";
			   }
			   else
			   {
					StatusString = "";
			   }

			   DisplayMonths = new ObservableCollection<MonthBudgetItemViewModel>(_Months);
		  }

	 }
}
