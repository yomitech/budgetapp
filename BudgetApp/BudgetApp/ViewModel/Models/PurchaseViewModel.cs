﻿using BudgetApp.Models;
using BudgetApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace BudgetApp.View
{
	 public class PurchaseViewModel : ViewModelBase
	 {
		  private string _Name;
		  private float _Price;
		  private DateTime _Date;
		  private PaymentMethod _PaymentMethod;

		  public string Name
		  {
			   get { return _Name; }
			   set { SetProperty(ref _Name, value); }
		  }

		  public float Price
		  {
			   get { return _Price; }
			   set { SetProperty(ref _Price, value); }
		  }

		  public DateTime Date
		  {
			   get { return _Date; }
			   set { SetProperty(ref _Date, value); }
		  }

		  public PaymentMethod PaymentMethod
		  {
			   get { return _PaymentMethod; }
			   set { SetProperty(ref _PaymentMethod, value); }
		  }

		  public PurchaseViewModel()
		  {
		  }
	 }
}
