﻿using BudgetApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace BudgetApp.ViewModel
{
    public class MonthBudgetItemViewModel : ViewModelBase
    {

		  private int _Id;
		  private float _TotalMoney;
		  private float _MoneySpent;
		  private float _BudgetLimit;
		  public float _StartingMoney;
		  private DateTime _Date;

		  public int Id
		  {
			   get { return _Id; }
			   set { SetProperty(ref _Id, value); }
		  }

		  public float TotalMoney
		  {
			   get { return _TotalMoney; }
			   set { SetProperty(ref _TotalMoney, value); }
		  }

		  public float MoneySpent
		  {
			   get { return _MoneySpent; }
			   set { SetProperty(ref _MoneySpent, value); }
		  }

		  public float BudgetLimit
		  {
			   get { return _BudgetLimit; }
			   set { SetProperty(ref _BudgetLimit, value); }
		  }

		  public float StartingMoney
		  {
			   get { return _StartingMoney; }
			   set { SetProperty(ref _StartingMoney, value); }
		  }

		  public DateTime Date
		  {
			   get { return _Date; }
			   set { SetProperty(ref _Date, value); }
		  }
	 }
}
