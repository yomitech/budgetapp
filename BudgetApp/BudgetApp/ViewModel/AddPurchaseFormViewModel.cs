﻿using BudgetApp.Models;
using BudgetApp.Services;
using BudgetApp.Validations;
using BudgetApp.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using System.Linq;
using System.Threading.Tasks;

namespace BudgetApp.ViewModel
{
	 public class AddPurchaseFormViewModel : ViewModelBase
	 {
		  private PurchaseViewModel _Purchase;
		  private readonly IPageService _PageService;
		  private readonly IDatabaseService _DatabaseService;
		  private ValidatableObject<string> _PurchaseName;
		  private ValidatableObject<string> _PurchasePrice;

		  public ObservableCollection<PaymentMethod> PaymentMethods { get; private set; }

		  public PurchaseViewModel Purchase
		  {
			   get { return _Purchase; }
			   set { SetProperty(ref _Purchase, value); }
		  }

		  public DateTime MinimumDate { get; private set; }
		  public DateTime MaximumDate { get; private set; }

		  public ValidatableObject<string> PurchaseName
		  {
			   get { return _PurchaseName; }
			   set { SetProperty(ref _PurchaseName, value); }
		  }

		  public ValidatableObject<string> PurchasePrice
		  {
			   get { return _PurchasePrice; }
			   set { SetProperty(ref _PurchasePrice, value); }
		  }

		  public ICommand AddCommand { get; private set; }
		  public ICommand ValidatePurchaseNameCommand { get; private set; }
		  public ICommand ValidatePurchasePriceCommand { get; private set; }

		  public AddPurchaseFormViewModel(IPageService pageService, IDatabaseService databaseService
			   ,MonthBudgetItemViewModel monthBudget)
		  {
			   _PageService = pageService;
			   _DatabaseService = databaseService;

			   PaymentMethods = new ObservableCollection<PaymentMethod>()
			   {
					new PaymentMethod(PaymentMethodType.Cash),
					new PaymentMethod(PaymentMethodType.CreditCard)
			   };

			   if(monthBudget == null)
			   {
					Task.Run(async () => { monthBudget = await GetCurrentOrLastMonth(); }).Wait(); 
			   }

			   Purchase = new PurchaseViewModel()
			   {
					Date = monthBudget != null ? (monthBudget.Date.Month != DateTime.Today.Month ? monthBudget.Date : DateTime.Today) : DateTime.Today,
					PaymentMethod = PaymentMethods.FirstOrDefault()
			   };


			   MinimumDate = new DateTime(_Purchase.Date.Year, _Purchase.Date.Month, 1);
			   MaximumDate = MinimumDate.AddMonths(1).AddDays(-1);

			   PurchaseName = new ValidatableObject<string>();
			   PurchasePrice = new ValidatableObject<string>
			   {
					Value = "0.0"
			   };

			   AddCommand = new Command(async () =>
			   {
					bool isValid = Validate();

					if (isValid)
					{
						 var newPurchase = new Purchase()
						 {
							  MonthBudgetId = monthBudget.Id,
							  Date = Purchase.Date.ToString(),
							  Name = PurchaseName.Value,
							  PaymentMethodType = Purchase.PaymentMethod.Type,
							  Price = float.Parse(PurchasePrice.Value)
						 };

						 await _DatabaseService.InsertAsync(newPurchase);

						 await UpdateMoney(monthBudget, newPurchase);
						 
						 // TODO: add loading icon?

						 // Go back to MonthBudgetItemDetailPage
						 await _PageService.PopModalAsync();
					}

			   });

			   ValidatePurchaseNameCommand = new Command(()=> { ValidatePurchaseName(); });
			   ValidatePurchasePriceCommand = new Command(()=> { ValidatePurchasePrice(); });


			   AddValidations();
		  }

		  // NOTE: Is this part of AddPurchaseForm responsibility?
		  private async Task<MonthBudgetItemViewModel> GetCurrentOrLastMonth()
		  {
			   var months = await _DatabaseService.GetTable<MonthBudgetItem>();

			   MonthBudgetItem currentOrLastMonth = null;

			   if (months.Count > 0)
					currentOrLastMonth = months[months.Count - 1];

			   if (currentOrLastMonth == null)
					return null;

			   var result = new MonthBudgetItemViewModel()
			   {
					Id = currentOrLastMonth.Id,
					Date = DateTime.Parse(currentOrLastMonth.Date),
					BudgetLimit = currentOrLastMonth.BudgetLimit,
					MoneySpent = currentOrLastMonth.MoneySpent,
					StartingMoney = currentOrLastMonth.StartingMoney,
					
			   };

			   return result;
		  }

		  private async Task UpdateMoney(MonthBudgetItemViewModel monthBudget, Purchase newPurchase)
		  {
			   monthBudget.MoneySpent += newPurchase.Price;
			   monthBudget.TotalMoney -= newPurchase.Price;

			   await _DatabaseService.QueryAsync<MonthBudgetItem>($"UPDATE MonthBudgetItem SET" +
					$" MoneySpent={monthBudget.MoneySpent},TotalMoney={monthBudget.TotalMoney} WHERE ID={monthBudget.Id}");

		  }

		  private bool Validate()
		  {
			   bool isValidUser = ValidatePurchaseName();
			   bool isValidPrice = ValidatePurchasePrice();

			   return isValidUser && isValidPrice;
		  }

		  private bool ValidatePurchaseName()
		  {
			   return PurchaseName.Validate();
		  }

		  private bool ValidatePurchasePrice()
		  {
			   return PurchasePrice.Validate();
		  }

		  private void AddValidations()
		  {
			   PurchaseName.Validations.Add(new IsNotNullOrEmptyRule<string>() { ValidateMessage = "A name is required." });
			   PurchasePrice.Validations.Add(new IsNotNullOrEmptyRule<string>() { ValidateMessage = "A price is required." });
			   PurchasePrice.Validations.Add(new IsNumericAndAboveZeroRule<string> { ValidateMessage = "A price has to be a number and above zero." });
		  }
	 }
}
