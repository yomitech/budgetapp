﻿using BudgetApp.Models;
using BudgetApp.Services;
using BudgetApp.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using System.Linq;

namespace BudgetApp.ViewModel
{
    public class MonthBudgetItemDetailViewModel : ViewModelBase
    {

		  private MonthBudgetItemViewModel _MonthBudgetItem;
		  private ObservableCollection<PurchaseViewModel> _Purchases;

		  private readonly IPageService _PageService;
		  private readonly IDatabaseService _DatabaseService;

		  public MonthBudgetItemViewModel MonthBudgetItem
		  {
			   get { return _MonthBudgetItem; }
			   set { SetProperty(ref _MonthBudgetItem, value); }
		  }

		  public ObservableCollection<PurchaseViewModel> Purchases
		  {
			   get { return _Purchases; }
			   set { SetProperty(ref _Purchases, value); }
		  }

		  public ICommand AddPurchaseCommand { get; private set; }
		  public ICommand LoadPurchasesCommand { get; private set; }

		  public MonthBudgetItemDetailViewModel(MonthBudgetItemViewModel monthBudgetItem, IPageService pageService,
			   IDatabaseService databaseService)
		  {
			   _MonthBudgetItem = monthBudgetItem;

			   _PageService = pageService;
			   _DatabaseService = databaseService;

			   Purchases = new ObservableCollection<PurchaseViewModel>();

			   LoadPurchasesFromDB();

			   AddPurchaseCommand = new Command(async () => {	
					
					await _PageService.PushModalAsync(new AddPurchaseFormPage(MonthBudgetItem));

			   });

			   LoadPurchasesCommand = new Command(LoadPurchasesFromDB);

		  }

		  private async void LoadPurchasesFromDB()
		  {
			   // Note: I use the id of Purchase only when read from the db, is this the only usage?
			   var purchasesFromDB = (await _DatabaseService.GetTable<Purchase>()).Where(p => p.MonthBudgetId == MonthBudgetItem.Id);

			   var purchases = new ObservableCollection<PurchaseViewModel>();

			   

			   foreach (var purchase in purchasesFromDB)
			   {
					var purchaseViewModel = new PurchaseViewModel
					{
						 // Note: I don't copy the id and monthBudgetId, maybe I will need it
						 Date = DateTime.Parse(purchase.Date),
						 Name = purchase.Name,
						 PaymentMethod = new PaymentMethod(purchase.PaymentMethodType),
						 Price = purchase.Price
					};

					purchases.Add(purchaseViewModel);
			   }

			   Purchases.Clear();
			   Purchases = new ObservableCollection<PurchaseViewModel>(purchases.OrderByDescending(p => p.Date));
		  }
    }
}
