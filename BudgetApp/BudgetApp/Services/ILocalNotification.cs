﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BudgetApp.Services
{
	 public interface ILocalNotification
	 {

		  void ShowNotificationEveryDay(int hour, int minute);

		  void CancelCurrentNotification();
	 }
}
