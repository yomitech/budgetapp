﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace BudgetApp.Services
{
	 public class SqliteDatabaseService : IDatabaseService
	 {
		  private ISqliteDb _SqliteDb;

		  public SqliteDatabaseService()
		  {
			   _SqliteDb = DependencyService.Get<ISqliteDb>();
		  }

		  private async Task<bool> DBCall<T>(T item, Func<object, Task<int>> DBFunc, SQLiteAsyncConnection connection) where T : new()
		  {
			   var sqliteConnection = _SqliteDb.GetConnection();

			   await sqliteConnection.CreateTableAsync<T>();

			   int numOfRows = await DBFunc(item);

			   if (numOfRows > 0)
					return true;

			   return false;
		  }

		  public async Task<bool> InsertAsync<T>(T item) where T : new()
		  {
			   var sqliteConnection = _SqliteDb.GetConnection();
			   return await DBCall(item, sqliteConnection.InsertAsync, sqliteConnection);
		  }

		  public async Task<bool> UpdateAsync<T>(T item) where T : new()
		  {
			   var sqliteConnection = _SqliteDb.GetConnection();
			   return await DBCall(item, sqliteConnection.UpdateAsync, sqliteConnection);
		  }

		  public async Task<bool> DeleteAsync<T>(T item) where T : new()
		  {
			   var sqliteConnection = _SqliteDb.GetConnection();
			   return await DBCall(item, sqliteConnection.DeleteAsync, sqliteConnection);
		  }

		  public async Task<List<T>> GetTable<T>() where T : new()
		  {
			   var sqliteConnection = _SqliteDb.GetConnection();

			   await sqliteConnection.CreateTableAsync<T>();

			   return await sqliteConnection.Table<T>().ToListAsync();
		  }

		  public async Task<List<T>> QueryAsync<T>(string query) where T : new()
		  {
			   var sqliteConnection = _SqliteDb.GetConnection();
			   return await sqliteConnection.QueryAsync<T>(query);
		  }
	 }
}
