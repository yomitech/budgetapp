﻿using BudgetApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BudgetApp.Services
{
    public interface IAuthenticationService
    {
		  bool IsAuthenticated { get; }

		  User AuthenticatedUser { get; }

		  Task<bool> LoginAsync(string email, string password);

		  //Task<bool> LoginWithFacebookAsync(FacebookResult result);

		  Task LogoutAsync();
    }
}
