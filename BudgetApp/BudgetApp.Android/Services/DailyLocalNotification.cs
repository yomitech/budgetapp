﻿using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using BudgetApp.Droid.Services;
using BudgetApp.Services;
using Java.Util;
using Xamarin.Forms;

[assembly: Dependency(typeof(DailyLocalNotification))]
namespace BudgetApp.Droid.Services
{
	 class DailyLocalNotification : ILocalNotification
	 {
		  private readonly int _RequestCode = 0;
		  private readonly AlarmManager _AlarmManager;
		  private readonly PendingIntent _PendingIntent;


		  public DailyLocalNotification()
		  {
			   var context = Android.App.Application.Context;
			   _AlarmManager = context.GetSystemService(Context.AlarmService) as AlarmManager;

			   Intent intent = new Intent(context, typeof(NotificationReceiver));

			   intent.PutExtra("message", "Update your today's expenses and incomes.");
			   intent.PutExtra("title", "Don't forget to keep track!");

			   _PendingIntent = PendingIntent.GetBroadcast(context, _RequestCode, intent, PendingIntentFlags.UpdateCurrent);

		  }

		  public void ShowNotificationEveryDay(int hour, int minute)
		  {
			   Calendar calendar = Calendar.GetInstance(Locale.Default);
			   calendar.Set(CalendarField.HourOfDay, hour);
			   calendar.Set(CalendarField.Minute, minute);

			   _AlarmManager.SetRepeating(AlarmType.RtcWakeup, calendar.TimeInMillis, AlarmManager.IntervalDay, _PendingIntent);

		  }

		  public void CancelCurrentNotification()
		  {
			   _AlarmManager.Cancel(_PendingIntent);
		  }
	 }
}