﻿using BudgetApp.View;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using BudgetApp.Droid;
using Xamarin.Facebook.Login.Widget;
using Android.Content;
using System.Collections.Generic;
using Android.Views;
using System;
using Xamarin.Facebook.Login;

[assembly: ExportRenderer(typeof(FacebookLoginButton), typeof(FacebookLoginButtonRenderer))]
namespace BudgetApp.Droid
{
	 public class FacebookLoginButtonRenderer : ViewRenderer<FacebookLoginButton, LoginButton>
	 {
		  LoginButton facebookLoginButton;

		  public FacebookLoginButtonRenderer(Context context) : base(context)
		  {

		  }

		  protected override void OnElementChanged(ElementChangedEventArgs<FacebookLoginButton> e)
		  {
			   base.OnElementChanged(e);

			   if(e.NewElement != null)
			   {
					e.NewElement.LogoutRequest += OnElementLogoutRequest;
			   }

			   if(Control == null || facebookLoginButton == null)
			   {
					facebookLoginButton = new LoginButton(Context);
					facebookLoginButton.SetReadPermissions(new List<string>() { "email", "user_photos", "public_profile" });
					SetNativeControl(facebookLoginButton);
			   }
		  }

		  private void OnElementLogoutRequest(object sender, EventArgs e)
		  {
			   LoginManager.Instance.LogOut();
		  }
	 }
}