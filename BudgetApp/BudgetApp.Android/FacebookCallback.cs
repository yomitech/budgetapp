﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Facebook;

namespace BudgetApp.Droid
{
	 public class FacebookCallback<T> : Java.Lang.Object, IFacebookCallback where T : Java.Lang.Object
	 {
		  public Action HandleCancel { get; set; }
		  public Action<FacebookException> HandleError { get; set; }
		  public Action<T> HandleSuccess { get; set; }

		  public void OnCancel()
		  {
			   HandleCancel?.Invoke();
		  }

		  public void OnError(FacebookException error)
		  {
			   HandleError?.Invoke(error);
		  }

		  public void OnSuccess(Java.Lang.Object result)
		  {
			   HandleSuccess?.Invoke(result.JavaCast<T>());
		  }
	 }
}