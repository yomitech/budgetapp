﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
 
namespace BudgetApp.Droid
{
	 [BroadcastReceiver]
	 [IntentFilter(new[]{ Intent.ActionBootCompleted})]
	 public class NotificationReceiver : BroadcastReceiver
	 {
		  public override void OnReceive(Context context, Intent intent)
		  {
			   var message = intent.GetStringExtra("message");
			   var title = intent.GetStringExtra("title");

			   Toast.MakeText(context, "Received intent!", ToastLength.Short).Show();

			   Intent notifIntent = new Intent(context, typeof(MainActivity));
			   notifIntent.PutExtra("page", "AddPurchaseFormPage");

			   PendingIntent pendingIntent = PendingIntent.GetActivity(context, 0, notifIntent, PendingIntentFlags.UpdateCurrent);

			   NotificationManager notificationManager = context.GetSystemService(Context.NotificationService) as NotificationManager;

			   var style = new NotificationCompat.BigTextStyle();
			   style.BigText(message);

			   NotificationCompat.Builder builder = new NotificationCompat.Builder(context).
					SetContentTitle(title).
					SetContentText(message).
					SetSmallIcon(Resource.Drawable.abc_btn_check_material).
					SetWhen(Java.Lang.JavaSystem.CurrentTimeMillis()).
					SetStyle(style).
					SetAutoCancel(true).
					SetContentIntent(pendingIntent);

			   var notification = builder.Build();
			   notificationManager.Notify(0, notification);
		  }
	 }
}