﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Content;
using Android.Support.V4.App;
using Java.Util;
using Xamarin.Facebook;
using Java.Lang;
using Xamarin.Facebook.Login;
using System.Collections.Generic;
using Org.Json;
using System.Threading.Tasks;
using System.Diagnostics;

using Debug = System.Diagnostics.Debug;
using Newtonsoft.Json;
using BudgetApp.Services;
using BudgetApp.Models;
using BudgetApp.ViewModel;
using BudgetApp.Helpers;
using System.Linq;

[assembly: MetaData("com.facebook.sdk.ApplicationId", Value = "@string/facebook_app_id")]

namespace BudgetApp.Droid
{
	 [Activity(Label = "BudgetApp", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	 public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity, GraphRequest.IGraphJSONObjectCallback
	 {
		  ICallbackManager _CallbackManager;
		  IDatabaseService _DatabaseService;

		  protected override void OnCreate(Bundle savedInstanceState)
		  {
			   TabLayoutResource = Resource.Layout.Tabbar;
			   ToolbarResource = Resource.Layout.Toolbar;

			   base.OnCreate(savedInstanceState);
			   global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
			   ImageCircle.Forms.Plugin.Droid.ImageCircleRenderer.Init();

			   _CallbackManager = CallbackManagerFactory.Create();
			   
			   FacebookLogin();

			   string page = Intent.GetStringExtra("page");

			   if (page == null)
					page = "Main";

			   LoadApplication(new App(page));

			   _DatabaseService = ViewModelLocator.Resolve<IDatabaseService>();
		  }

		  private void FacebookLogin()
		  {
			   AccessToken accessToken = AccessToken.CurrentAccessToken;
			   bool isLoggedIn = accessToken != null && !accessToken.IsExpired;

			   LoginManager.Instance.RegisterCallback(_CallbackManager, new FacebookCallback<LoginResult>
			   {
					HandleSuccess = async (loginResult) =>
					{

						 BudgetApp.Helpers.Settings.FacebookAccessToken = AccessToken.CurrentAccessToken.Token;


						 GraphRequest request = GraphRequest.NewMeRequest(loginResult.AccessToken, this);

						 Bundle bundle = new Bundle();
						 bundle.PutString("fields", "id,name,email,picture,photos");
						 request.Parameters = bundle;

						 await request.ExecuteAsync().GetAsync();


					},
					HandleCancel = () =>
					{

					},
					HandleError = loginError =>
					{
						 Debug.WriteLine(loginError);
					}
			   });

		  }

		  protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
		  {
			   base.OnActivityResult(requestCode, resultCode, data);

			   _CallbackManager.OnActivityResult(requestCode, (int)resultCode, data);
		  }

		  public async void OnCompleted(JSONObject @object, GraphResponse response)
		  {
			   if (response.Error != null)
			   {
					Debug.WriteLine(response.Error.ErrorMessage);
			   }
			   else
			   {
					var jsonString = @object.ToString();

					FacebookResult result = JsonConvert.DeserializeObject<FacebookResult>(jsonString);

					await LoginWithFacebook(result);
			   }
		  }

		  private async Task LoginWithFacebook(FacebookResult result)
		  {
			   User user = new User()
			   {
					Name = result.Name,
					Email = result.Email,
					ImageUrl = Profile.CurrentProfile.GetProfilePictureUri(200, 200).ToString(),
					FacebookUserId = result.Id
			   };

			   var userDB = (await _DatabaseService.GetTable<User>()).Where(u => (u.FacebookUserId == user.FacebookUserId)).FirstOrDefault();

			   if (userDB == null)
			   {
					await _DatabaseService.InsertAsync(user);
			   }
			   else
			   {
					// TODO: Check if it's doing something
					user.Id = userDB.Id;
					await _DatabaseService.UpdateAsync(user);
			   }

			   Settings.UserId = user.Id;
			   Settings.IsLoggedIn = true;

			   App.GoToPage();
		  }
	 }
}