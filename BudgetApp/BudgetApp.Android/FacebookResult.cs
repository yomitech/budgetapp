﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace BudgetApp.Droid
{
	 public class Data
	 {
		  public int height { get; set; }
		  public bool is_silhouette { get; set; }
		  public string url { get; set; }
		  public int width { get; set; }
	 }

	 public class Picture
	 {
		  public Data data { get; set; }
	 }

	 public class Datum
	 {
		  public DateTime created_time { get; set; }
		  public string name { get; set; }
		  public string id { get; set; }
	 }

	 public class Cursors
	 {
		  public string before { get; set; }
		  public string after { get; set; }
	 }

	 public class Paging
	 {
		  public Cursors cursors { get; set; }
	 }

	 public class Photos
	 {
		  public IList<Datum> data { get; set; }
		  public Paging paging { get; set; }
	 }

	 public class FacebookResult
	 {
		  public string Id { get; set; }
		  public string Name { get; set; }
		  public string Email { get; set; }
		  public Picture Picture { get; set; }
		  public Photos Photos { get; set; }
	 }

}