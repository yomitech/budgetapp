﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Diagnostics;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using BudgetApp.Droid.Presistance;
using BudgetApp.Services;
using SQLite;

	 
[assembly: Xamarin.Forms.Dependency(typeof(SqliteDb))]

namespace BudgetApp.Droid.Presistance
{
	 public class SqliteDb : ISqliteDb
	 {
		  public static string dbFile = "budgetDB.db3";

		  public SQLiteAsyncConnection GetConnection()
		  {
			   var documentPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData);
			   var path = Path.Combine(documentPath, dbFile);

			   System.Diagnostics.Debug.WriteLine(path);

			   return new SQLiteAsyncConnection(path);
		  }
	 }
}