﻿using System;
using BudgetApp.Models;
using BudgetApp.Services;
using BudgetApp.ViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NUnit.Framework;
using Xamarin.Forms.Mocks;

namespace BudgetApp.Tests.ViewModels
{
	 [TestFixture()]
	 public class AddMonthViewModelTests
	 {
		  private AddMonthFormViewModel _ViewModel;
		  private Mock<IPageService> _PageService;
		  private Mock<IDatabaseService> _DatabaseService;

		  [SetUp]
		  public void Setup()
		  {
			   MockForms.Init();

			   _PageService = new Mock<IPageService>();
			   _DatabaseService = new Mock<IDatabaseService>();

			   _ViewModel = new AddMonthFormViewModel(_PageService.Object, _DatabaseService.Object);
		  }

		  [Test]
		  public void ValidateDate_MonthAlreadyExistes_ReturnFalse()
		  {

			   // _ViewModel.

		  }

		  [Test]
		  public void ValidateDate_YearIsInTheFuture__ReturnFalse()
		  {

			   _ViewModel.Year = DateTime.Now.AddYears(1).Year;
			   _ViewModel.Month = DateTime.Now.ToString();

			   NUnit.Framework.Assert.True(!_ViewModel.DateErrorValid);

		  }

		  [Test]
		  public void ValidateDate_MonthIsInTheFuture__ReturnFalse()
		  {

			   _ViewModel.Year = DateTime.Now.Year;
			   _ViewModel.Month = DateTime.Now.AddMonths(1).ToString();

			   NUnit.Framework.Assert.True(!_ViewModel.DateErrorValid);

		  }

		  [Test]
		  public void Add_CorrectInput_InsertNewMonthToDB()
		  {
			   _ViewModel.BudgetLimitValidation.Value = "100";
			   _ViewModel.StartingMoneyValidation.Value = "100";

			   _ViewModel.AddMonthCommand.Execute(null);

			   _DatabaseService.Verify(e => e.InsertAsync(It.IsAny<MonthBudgetItem>()));
		  }

		  [Test]
		  public void ValidateBudgetLimit_NotANumber_ReturesFalse()
		  {
			   _ViewModel.BudgetLimitValidation.Value = "test";

			   _ViewModel.ValidateBudgetLimitCommand.Execute(null);

			   NUnit.Framework.Assert.False(_ViewModel.BudgetLimitValidation.IsValid);

		  }
	 }
}
