﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Xamarin.Forms.Mocks;
using BudgetApp.ViewModel;
using BudgetApp.Services;
using Moq;
using BudgetApp.Models;

namespace BudgetApp.Tests.ViewModels
{
	 [TestFixture()]
	 public class AddPurchaseViewModelTests
	 {
		  private AddPurchaseFormViewModel _AddPurchaseViewModel;
		  private Mock<IPageService> _PageService;
		  private Mock<IDatabaseService> _DatabaseService;
		  private MonthBudgetItemViewModel _MonthBudgetItem;

		  [SetUp]
		  public void Setup()
		  {
			   MockForms.Init();

			   _MonthBudgetItem = new MonthBudgetItemViewModel()
			   {
					Id = 0,
					BudgetLimit = 300,
					Date = DateTime.Today,
					MoneySpent = 100,
					TotalMoney = 5000
			   };

			   _PageService = new Mock<IPageService>();
			   _DatabaseService = new Mock<IDatabaseService>();

			   _AddPurchaseViewModel = new AddPurchaseFormViewModel(_PageService.Object, _DatabaseService.Object , _MonthBudgetItem);

		  }

		  [Test]
		  public void ValidatePurchaseName_PurchaseNameIsCorrect_ReturnTrue()
		  {
			   _AddPurchaseViewModel.PurchaseName.Value = "Test";

			   _AddPurchaseViewModel.ValidatePurchaseNameCommand.Execute(null);

			   NUnit.Framework.Assert.IsTrue(_AddPurchaseViewModel.PurchaseName.IsValid);

		  }

		  [Test]
		  public void ValidatePurchaseName_PurchaseNameIsEmpty_ReturnFalse()
		  {
			   _AddPurchaseViewModel.PurchaseName.Value = "";

			   _AddPurchaseViewModel.ValidatePurchaseNameCommand.Execute(null);

			   NUnit.Framework.Assert.IsFalse(_AddPurchaseViewModel.PurchaseName.IsValid);

		  }

		  [Test]
		  public void ValidatePurchaseName_PurchaseNameIsSpace_ReturnFalse()
		  {
			   _AddPurchaseViewModel.PurchaseName.Value = " ";

			   _AddPurchaseViewModel.ValidatePurchaseNameCommand.Execute(null);

			   NUnit.Framework.Assert.IsFalse(_AddPurchaseViewModel.PurchaseName.IsValid);

		  }

		  [Test]
		  public void ValidatePurchasePrice_PurchasePriceIsCorrect_ReturnTrue()
		  {
			   _AddPurchaseViewModel.PurchasePrice.Value = "123";

			   _AddPurchaseViewModel.ValidatePurchasePriceCommand.Execute(null);

			   NUnit.Framework.Assert.IsTrue(_AddPurchaseViewModel.PurchasePrice.IsValid);

		  }

		  [Test]
		  public void ValidatePurchasePrice_PurchasePriceIsEmpty_ReturnFalse()
		  {
			   _AddPurchaseViewModel.PurchasePrice.Value = "";

			   _AddPurchaseViewModel.ValidatePurchasePriceCommand.Execute(null);

			   NUnit.Framework.Assert.IsFalse(_AddPurchaseViewModel.PurchasePrice.IsValid);

		  }

		  [Test]
		  public void ValidatePurchasePrice_PurchasePriceIsSpace_ReturnFalse()
		  {
			   _AddPurchaseViewModel.PurchasePrice.Value = " ";

			   _AddPurchaseViewModel.ValidatePurchasePriceCommand.Execute(null);

			   NUnit.Framework.Assert.IsFalse(_AddPurchaseViewModel.PurchasePrice.IsValid);

		  }

		  [Test]
		  public void ValidatePurchasePrice_PurchasePriceIsNotANumber_ReturnFalse()
		  {
			   _AddPurchaseViewModel.PurchasePrice.Value = "test";

			   _AddPurchaseViewModel.ValidatePurchasePriceCommand.Execute(null);

			   NUnit.Framework.Assert.IsFalse(_AddPurchaseViewModel.PurchasePrice.IsValid);
		  }

		  [Test]
		  public void Add_CorrectInput_InsertNewPurchaseToDB()
		  {
			   _AddPurchaseViewModel.PurchaseName.Value = "Test";
			   _AddPurchaseViewModel.PurchasePrice.Value = "123";

			   _AddPurchaseViewModel.AddCommand.Execute(null);

			   _DatabaseService.Verify(e => e.InsertAsync(It.IsAny<Purchase>()));
		  }

		  [Test]
		  public void Add_WrongPurchaseName_NotInsertNewPurchaseToDB()
		  {
			   _AddPurchaseViewModel.PurchaseName.Value = "";
			   _AddPurchaseViewModel.PurchasePrice.Value = "123";

			   _AddPurchaseViewModel.AddCommand.Execute(null);

			   _DatabaseService.Verify(e => e.InsertAsync(It.IsAny<Purchase>()), Times.Never());
		  }

		  [Test]
		  public void Add_WrongPurchasePrice_NotInsertNewPurchaseToDB()
		  {
			   _AddPurchaseViewModel.PurchaseName.Value = "Test";
			   _AddPurchaseViewModel.PurchasePrice.Value = "";

			   _AddPurchaseViewModel.AddCommand.Execute(null);

			   _DatabaseService.Verify(e => e.InsertAsync(It.IsAny<Purchase>()), Times.Never());
		  }

		  [Test]
		  public void Add_CorrectInput_ShouldNavigateTheUserToMonthBudgetItemDetailPage()
		  {
			   _AddPurchaseViewModel.PurchaseName.Value = "Test";
			   _AddPurchaseViewModel.PurchasePrice.Value = "123";

			   _AddPurchaseViewModel.AddCommand.Execute(null);

			   _PageService.Verify(p => p.PopModalAsync());
		  }

		  [Test]
		  public void Add_WrongPurchaseName_ShouldNavigateTheUserToMonthBudgetItemDetailPage()
		  {
			   _AddPurchaseViewModel.PurchaseName.Value = "";
			   _AddPurchaseViewModel.PurchasePrice.Value = "123";

			   _AddPurchaseViewModel.AddCommand.Execute(null);

			   _PageService.Verify(p => p.PopModalAsync(), Times.Never());
		  }

		  [Test]
		  public void Add_WrongPurchasePrice_ShouldNavigateTheUserToMonthBudgetItemDetailPage()
		  {
			   _AddPurchaseViewModel.PurchaseName.Value = "Test";
			   _AddPurchaseViewModel.PurchasePrice.Value = "";

			   _AddPurchaseViewModel.AddCommand.Execute(null);

			   _PageService.Verify(p => p.PopModalAsync(), Times.Never());
		  }

		  [Test]
		  public void Add_ShouldUpdateMoney()
		  {
			   _AddPurchaseViewModel.PurchaseName.Value = "Test";
			   _AddPurchaseViewModel.PurchasePrice.Value = "123";

			   _AddPurchaseViewModel.AddCommand.Execute(null);

			   _DatabaseService.Verify(d => d.QueryAsync<MonthBudgetItem>(It.IsAny<string>()));
		  }
	 }
}
